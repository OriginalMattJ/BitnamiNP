# Bitnami NP (New Project)

  This is a Python script that creates the boilerplate code to create projects in Bitnami's WAMP stack. This might work on other OS or other stacks, but hasn't been tested.

  Contact me at __mdjacks@bgsu.edu__ with any comments, questions, or concerns.

## First Time Setup

  You will need to be able to run python scripts.

  You will want to makes sure BNP is up to date on your Bitnami setup. The first time you run BNP, use the -d and -v switches (more info in _Options_)  as well as the -n switch. 

  See _error handling_ for more info.

## Creating a new project

  1. Navigate the command line to the directory storing the script 
  2. Type `bitnamiNP.py -n [Project Name]` where `[Proejct Name]` is the name of your new project. Note this is case sensitive.
      See _options_ for more available switches
  3. Enter the desired project name when prompted
  4. Deal with any errors, See _error handling_ for more info.
  5. Restart the Apache web server
  6. Navigate your borwser to `localhost/[Project Name]/index.php`


## Options
  + Name: `-n` or `-name`

    Description: takes in an argument for the project name. This switch ___is required___, forgetting it will throw you errors.

    Value Required: Yes

  + Empty: `-e` or `-empty`

    Description: Gives the project an empty directory. Use this if you are copying in an existing project from elsewhere, like GitLab.

    Value Required: No

  + Version: `-v` or `-version`

    Description: Takes the currently installed Bitnami version as an argument, updates the boilerplate code and how the server config files are updated.

    Value Required: Yes

  + Directory: `-d` or `-directory`

    Description: Takes the _absolute_ path to Bitnami's directory. On a Window's machine using version `7.1.21-0` it would be 

        C:\Bitnami\wampstack-7.1.21-0\

    The contents of this directory should be apache2, apps, bnsupport, common, etc.

    __Nota Bene:__ this path _needs to end_ with a slash, or it will throw an error

    Value Required: Yes


## Error Handling

+ 'Bitnami directory not found'


  Issue: Bitnami's install path or version number has changed. 

  Fix: Use the `-v` and `-d` switches (See _Options_ for more) with the correct information.

  Nota Bene: Don't worry about back slashes, they are delt with prooperly.

   + 404 Error


    Issue: Browser can't find the specified folder

    Fix: Ensure the server is restarted and the URL is `localhost\[Project Name]\index.php` exactly. This is case sensitive.

  +  Permission Denied (Errno 13)


    Issue: User doesn't have the required permissions to do the requested actions

    Fix: _Unkown_, but I'm working on it. Let me know if you have any thoughts or ideas.

  + Unable to find Bitnami server config files

    Issue: BNP is loking for Bitnami's server config files in the wrong directory.

    Fix: Run BNP with the `-d` switch (See _Options_ for more info), ensure that you end the directory location with a `\`. BNP will create the directory if it isn't found, so you will need to delete the new folder tree. BNP wil display directory names, use these to see where the directory location is wrong. 

## Other Notes

  + The directory `./bin/testLocation/` is a mockup of the files and directories that get changed on execution. If you change any code, you might want to test it here.

  + The code is pulled from `./bin/boilerplate/`, any changes that you make to that directory will be reflected onto all future projects.