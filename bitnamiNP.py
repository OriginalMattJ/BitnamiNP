import pickle
from distutils.dir_util import copy_tree
import os
import sys
import getopt


#global variables
boilerplateLocation = ".\\bin\\boilerplate\\"
placeHolder = 'demo'
directoryData = '.\\bin\\data\\'

def testBaseDirExists(dirLocation):
	if not os.path.isdir(dirLocation):
		print("Directory: " + dirLocation + " doesn't exist.")

		newLocation = input("New Location (See https://gitlab.com/OriginalMattJ/BitnamiNP for info): ")
		file = open(directoryData + "baseDirectory", "wb")
		pickle.dump(newLocation, file)
		file.close()

		exit()

def readFiles():
	data = {
		'baseDirectory':   "",
		'version':		   "",
		'appsDirectory':   "",
		'appsConf':		   "",
		'updateAppFiles':  [],

		'serverDirectory': "",
		'updateSvrFiles':  []}

	for key in data:
		data[key] = readPickle(key)

	return data

def readPickle(pickleName):
	fileName = directoryData + pickleName
	fileObject = open( fileName, 'rb')
	data = pickle.load(fileObject)
	fileObject.close()

	return data

# searchAndReplace finds every instance of placeHolder in file and replaces it with newStr
def searchAndReplace(placeHolder, newStr, fileDir):
	file = open(fileDir, 'r')
	fileData = file.read()
	file.close()

	newData = fileData.replace(placeHolder, newStr)

	file = open(fileDir, 'w')
	file.write(newData)
	file.close()


def append(text, file):
	try:
		code = open(file, 'a')
		code.write(text)
	except Exception as e:
		print("\n\nERROR: Unable to find the following file. See README.md for more info.\n\t ", end="")
		print(file)
		exit()
	


def translateArgs():
	projectName  = "new_proj"
	emptyProject = False
	version      = False
	directory    = False

	cmdArgs = sys.argv
	argList = cmdArgs[1:]

	#Define switches
	unixOptions = "n:ev:d:"
	gnuOptions  = ["name=", "empty", "version=", "directory="]

	# Parse arguments
	try:
		arguments, vaules = getopt.getopt(argList, unixOptions, gnuOptions)
	except getopt.error as e:
		print(str(e))
		sys.exit(2)

	# Interpret Arguments
	for currentArg, currentValue in arguments:
		if currentArg   in ("-n", "-name"):
			projectName = currentValue
		elif currentArg in ("-e", "-empty"):
			emptyProject = True
		elif currentArg in ("-v", "-version"):
			version = currentValue
		elif currentArg in ("-d", "-directory"):
			directory = currentValue 
		else:
			print(("Unhandled argument: %s") % (currentArg))

	optsDict = {'name': projectName, 'empty': emptyProject, 'version': version, 'directory': directory}

	return optsDict


def updatePickle(pickleName, data):
	fileName = directoryData + pickleName
	fileObject = open( fileName, 'wb')
	pickle.dump(data, fileObject)
	fileObject.close()
	print(pickleName+': '+data)


def main():
	options = translateArgs()

	# Version option
	if options['version'] != False:
		oldVersion = readPickle('version')
		newVersion = options['version']

		print(oldVersion)

		updatePickle('version', options['version'])

		for i in ['httpd-app.conf', 'httpd-prefix.conf', 'httpd-vhosts.conf']:
			fileName = boilerplateLocation + 'demo\\conf\\' + i

			print(fileName)

			searchAndReplace(oldVersion, newVersion, fileName)

	# Directory Options
	if options['directory'] != False:
		updatePickle('baseDirectory', options['directory'])




	# Generate Project
	print("Generating directory names....", end="")

	data = readFiles()

	testBaseDirExists(data['baseDirectory'])

	appsLocation = str(data['baseDirectory']) + str(data['appsDirectory'])

	serverLocation = str(data['baseDirectory']) + str(data['serverDirectory'])

	# apply arguments to code
	try:
		appConfigLocation = appsLocation + options['name'] + '\\' + data['appsConf']
	except:
		print("\nERROR: Missing -n switch")
		exit()

	print("Done.")
	

	# Check if project name exists
	projectLocation = appsLocation + options['name']
	if os.path.isdir(projectLocation):
		print("\nERROR: Project name is unavailable")
		exit()

	

	# If User wants a complete new project
	if not options['empty']:
		# Copy code to correct location
		print("Copying project....", end="")

		copy_tree(boilerplateLocation, appsLocation)

		print("Done.")
		# If location is incorrect
			# Let user enter new location and save through pickle

		# Update code to include project name
		# Rename folder
		print("Updating file names....", end="")

		os.rename(appsLocation + placeHolder, appsLocation + options['name'])

		print("Done.")
		# Update file contents
		# SEARCH AND REPLACE in Project Files
		print("Updating project file contents....", end="")

		for i in data['updateAppFiles']:
			searchAndReplace(placeHolder, options['name'], appConfigLocation+i)

		print("Done.")

	else:
		print("Creating empty project....", end="")

		os.mkdir(appsLocation + projectName)

		print("Done.")

	# APPEND in Bitnami Files
	print("Updating Bitnami file contents....", end="")

	serverConf = data['baseDirectory'] + data['serverDirectory']

	appendStr = '\nInclude "' + appConfigLocation + 'httpd-prefix.conf"'

	for i in data['updateSvrFiles']:
		append(appendStr, serverConf+i)

	print("Done.")

if __name__ == '__main__':
	main()