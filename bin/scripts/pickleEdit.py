import pickle
import os
# C:\\Bitnami\\wampstack-7.1.21-0\\
# .\\bin\\testLocation\\
def main():
	data = {
		'baseDirectory':   "C:\\Bitnami\\wampstack-7.1.25-0\\",
		'appsDirectory':   "apps\\",
		'appsConf':		   "conf\\",
		'updateAppFiles':  ["httpd-app.conf",
							"httpd-prefix.conf"],

		'serverDirectory': "apache2\\conf\\bitnami\\",
		'updateSvrFiles':  ["bitnami-apps-prefix.conf"]}

	for key in data:
		fileName = "../data/" + key
		fileObject = open( fileName, 'wb')
		pickle.dump(data[key], fileObject)
		fileObject.close()
		print(key, ': ', data[key])



if __name__ == '__main__':
	main()