


class fileHandling():

	_dataHash = {}

	def __init__(self, dataHash):
		self._dataHash = dataHash

	# searchAndReplace finds every instance of placeHolder in file and replaces it with newStr
	def searchAndReplace(self, placeHolder, newStr, fileDir):
		file = open(fileDir, 'r')
		fileData = file.read()
		file.close()

		newData = fileData.replace(placeHolder, newStr)

		file  = open(fileDir, 'w')
		file.write(newData)
		file.close()

	def append(self, text, file):
		code = open(file, 'a')
		code.write(text)

