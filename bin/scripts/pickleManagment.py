import pickle


def readFiles(dir):
	data = {
		'baseDirectory':   "",
		'appsDirectory':   "",
		'appsConf':		   "",
		'updateAppFiles':  [],

		'serverDirectory': "",
		'updateSvrFiles':  []}

	for key in data:
		fileName = dir + key
		fileObject = open( fileName, 'rb')
		data[key] = pickle.load(fileObject)
		fileObject.close()

	return data